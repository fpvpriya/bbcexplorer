//
//  Character.swift
//  BBCExplorer
//
//

import Foundation

public struct Character: Codable {
    let name: String
    let imageString: String
    let occupation: [String]
    let status: String
    let nickname: String
    let seasonAppearance: [Int]
    
    enum CodingKeys: String, CodingKey {
        case name
        case imageString = "img"
        case occupation
        case status
        case nickname
        case seasonAppearance = "appearance"
    }
    
    init(name: String, imageString: String, occupation: [String], status: String, nickname: String, seasonAppearance: [Int]) {
        self.name = name
        self.imageString = imageString
        self.occupation = occupation
        self.status = status
        self.nickname = nickname
        self.seasonAppearance = seasonAppearance
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = (try? container.decode(String.self, forKey: .name)) ?? ""
        imageString = (try? container.decode(String.self, forKey: .imageString)) ?? ""
        occupation = (try? container.decode([String].self, forKey: .occupation)) ?? [""]
        status = (try? container.decode(String.self, forKey: .status)) ?? ""
        nickname = (try? container.decode(String.self, forKey: .nickname)) ?? ""
        seasonAppearance = (try? container.decode([Int].self, forKey: .seasonAppearance)) ?? [-1]
    }
}
