//
//  URLPath.swift
//  BBCExplorerTests
//
//


enum URLPath: String {
    case characters
    
    func path() -> String {
        switch self {
        case .characters:
            return "https://breakingbadapi.com/api/characters"
        }
    }
}

