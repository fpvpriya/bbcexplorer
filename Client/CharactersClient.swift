//
//  CharactersClient.swift
//  BBCExplorer
//
//

import UIKit

typealias QueryResult = (_ characters: [Character]?, _ errorMessage: String?) -> Void

protocol CharactersClientType {
    func getCharacters(onCompletion: @escaping QueryResult)
}

class CharactersClient: CharactersClientType {
    var fetchData: RequestNetworkType!

    init(network: RequestNetworkType) {
        self.fetchData = network
    }
    
    //MARK: - Fetch characters
     func getCharacters(onCompletion: @escaping QueryResult) {
        fetchData.requestData(URLPath.characters.path()) { (data, errorMessage) in
            if let data = data {
                onCompletion(self.parseCharacters(data), nil)
            } else {
                onCompletion(nil, errorMessage)
            }
        }
    }
    
    //MARK: - Parse
    
    private func parseCharacters(_ data: Data) -> [Character] {
        let decoder = JSONDecoder()
        let characters = try! decoder.decode([Character].self, from: data)
        return characters
    }
}

