//
//  SearchControllerViewModelTests.swift
//  BBCExplorerTests
//
//

import XCTest
@testable import BBCExplorer

class SearchControllerViewModelTests: XCTestCase {
    
    func testFilterContent_searchTextFilter0_countIs1() {
        sut.setAllCharacters(characters)
        sut.filterContentForSearchText("Nick", false, 0)
        XCTAssertEqual(1, charactersViewModel.tableViewViewModel.allCharacters().count)
    }
    
    func testFilterContent_searchTextFilter1_countIs1() {
        sut.setAllCharacters(characters)
        sut.filterContentForSearchText("Nick", false, 1)
        XCTAssertEqual(1, charactersViewModel.tableViewViewModel.allCharacters().count)
    }

    func testFilterContent_searchTextFilter4_countIs1() {
        sut.setAllCharacters(characters)
        sut.filterContentForSearchText("Nick", false, 4)
        XCTAssertEqual(0, charactersViewModel.tableViewViewModel.allCharacters().count)
    }
    
    func testFilterContent_searchEmptyFilter0_countIs2() {
        sut.setAllCharacters(characters)
        sut.filterContentForSearchText("", true, 0)
        XCTAssertEqual(2, charactersViewModel.tableViewViewModel.allCharacters().count)
    }
    
    func testFilterContent_searchEmptyFilter2_countIs2() {
        sut.setAllCharacters(characters)
        sut.filterContentForSearchText("", true, 2)
        XCTAssertEqual(2, charactersViewModel.tableViewViewModel.allCharacters().count)
    }
    
    func testFilterContent_searchEmptyFilter4_countIs1() {
        sut.setAllCharacters(characters)
        sut.filterContentForSearchText("", true, 4)
        XCTAssertEqual(1, charactersViewModel.tableViewViewModel.allCharacters().count)
    }

    override func setUp() {
        super.setUp()
        mockClient = MockCharactersClient()
//        let tableViewViewModel = CharactersDelegateDataSourceViewModel(parent: charactersViewModel)
        charactersViewModel = CharactersViewModel(client: mockClient, navigationController: navigationController)
        _ = charactersViewModel.createDelegateDatasource()
        sut = SearchControllerViewModel(parent: charactersViewModel)
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    var characters = Character.fakeCharacters()
    var sut: SearchControllerViewModel!
    var mockClient: MockCharactersClient!
    let navigationController = UINavigationController()
    var charactersViewModel: CharactersViewModel!
}

