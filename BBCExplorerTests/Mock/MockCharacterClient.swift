//
//  MockCharacterClient.swift
//  BBCExplorerTests
//
//

import UIKit
@testable import BBCExplorer

class MockCharactersClient: CharactersClientType {
    struct Mocks {
        var characters: [Character]?
        var errorMessage: String?
    }
    
    func getCharacters(onCompletion: @escaping QueryResult) {
        return onCompletion(mocks.characters, mocks.errorMessage)
    }
    
    // MARK: - Stored Properties
    
    var mocks = Mocks()
}
