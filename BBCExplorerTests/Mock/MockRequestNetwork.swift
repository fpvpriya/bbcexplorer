//
//  MockRequestNetwork.swift
//  BBCExplorerTests
//
//

import UIKit
@testable import BBCExplorer

class MockRequestNetwork: RequestNetworkType {
    struct Mocks {
        var data: Data?
        var errorMessage: String?
    }
    
    func requestData(_ from: String, onCompletion: @escaping DataResult) {
        return onCompletion(mocks.data, mocks.errorMessage)
    }
    
    var mocks = Mocks()
    
}
