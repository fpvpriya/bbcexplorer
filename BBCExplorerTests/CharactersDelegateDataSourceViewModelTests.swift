//
//  CharactersDelegateDataSourceViewModelTests.swift
//  BBCExplorerTests
//
//

import XCTest
@testable import BBCExplorer

class CharactersDelegateDataSourceViewModelTests: XCTestCase {
    
    func testSetCharacters() {
        sut.setCharacters(self.characters)
        XCTAssertEqual("Alive", sut.allCharacters().first?.status)
        XCTAssertEqual("Alive James", sut.allCharacters().last?.status)
    }
    
    func testNumberOfCharacters() {
        sut.setCharacters(self.characters)
        XCTAssertEqual(2, sut.allCharacters().count)
    }
    
    func testNameAtRow() {
        sut.setCharacters(self.characters)
        XCTAssertEqual("Nick", sut.nameAtRow(0))
        XCTAssertEqual("James", sut.nameAtRow(1))
    }
    
    func testNameAtRow_whenEmpty() {
        sut.setCharacters([])
        XCTAssertEqual("", sut.nameAtRow(0))
        XCTAssertEqual("", sut.nameAtRow(1))
    }
    
    func testimageURLAtRow_whenEmpty() {
        sut.setCharacters([])
        XCTAssertNil(sut.imageURLAtRow(0))
    }
    
    func testImageURLAtRow_whenNotEmpty() {
        sut.setCharacters(self.characters)
        XCTAssertNotNil(sut.imageURLAtRow(0))
        XCTAssertEqual(URL(string: "image"), sut.imageURLAtRow(0))
    }
    
    func testCharacterAtRow() {
        sut.setCharacters(self.characters)
        XCTAssertEqual("Nick1", sut.characterAtRow(0)?.nickname)
    }
    
    func testCharacterAtRow_whenEmpty() {
        sut.setCharacters([])
        XCTAssertNil(sut.characterAtRow(0))
    }
    
    override func setUp() {
        super.setUp()
        mockClient = MockCharactersClient()
        let charactersViewModel = CharactersViewModel(client: mockClient, navigationController: navigationController)
        sut = CharactersDelegateDataSourceViewModel(parent: charactersViewModel)
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    var characters = Character.fakeCharacters()
    var sut: CharactersDelegateDataSourceViewModel!
    var mockClient: MockCharactersClient!
    let navigationController = UINavigationController()
}

