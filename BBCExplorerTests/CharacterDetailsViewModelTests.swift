//
//  CharacterDetailsViewModelTests.swift
//  BBCExplorerTests
//
//  Created by Vishnu Priya on 06/12/2019.
//

import XCTest
@testable import BBCExplorer

class CharacterDetailsViewModelTests: XCTestCase {
    
    func testCharacterName() {
        sut.setCharacter(self.character)
        XCTAssertEqual("Nick ( Nick1 ) ", sut.characterName())
    }
    
    func testCharacterOccupation() {
        sut.setCharacter(self.character)
        XCTAssertEqual("One, Two", sut.characterOccupation())
    }
    
    func testCharacterStatus() {
        sut.setCharacter(self.character)
        XCTAssertEqual("Alive", sut.characterStatus())
    }
    
    func testCharacterSeasons() {
        sut.setCharacter(self.character)
        XCTAssertEqual("1, 2, 3", sut.characterSeasons())
    }
    
    func testCharacteImageURL() {
        sut.setCharacter(self.character)
        XCTAssertEqual(URL(string: "image"), sut.characterImageURL())
    }

    
    override func setUp() {
        super.setUp()
        sut = CharacterDetailsViewModel()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    let character = Character.fake()
    var sut: CharacterDetailsViewModel!

}
