//
//  Fakeable.swift
//  BBCExplorerTests
//
//  Created by Vishnu Priya on 05/12/2019.
//

import Foundation
import Fakery
import XCTest
@testable import BBCExplorer

let faker = Faker(locale: "en_GB")

protocol Fakeable {
    static func fake() -> Self
}

extension Character: Fakeable {
    static func fake() -> Character {
        return .init(name: "Nick", imageString: "image", occupation: ["One", "Two"], status: "Alive", nickname: "Nick1", seasonAppearance: [1, 2, 3])
    }
    
    static func fakeCharacters() -> [Character] {
        return [.init(name: "Nick", imageString: "image", occupation: ["One", "Two"], status: "Alive", nickname: "Nick1", seasonAppearance: [1, 2, 3]),
                .init(name: "James", imageString: "https://image1", occupation: ["One", "Two", "Three"], status: "Alive James", nickname: "James1", seasonAppearance: [1, 2, 3, 4, 5])]
        
    }
}
