//
//  CharacterClientTests.swift
//  BBCExplorerTests
//
//  Created by Vishnu Priya on 06/12/2019.
//

import XCTest
@testable import BBCExplorer

class CharactersClientTests: XCTestCase {

    func resourceResponse(from resource: String) -> Data {
        guard
            let url = Bundle(for: type(of: self)).url(forResource: resource, withExtension: "json"),
            let data = try? Data(contentsOf: url) else {
                fatalError("Could not load JSON resource named \(resource) in the test bundle.")
        }
        
        return data
    }
    
    func testRequestData_IsSuccess() {
        let expectation = self.expectation(description: "Fetch character success")
        let data = resourceResponse(from: "Characters")
        mockRequestNetwork.mocks.data = data
        mockRequestNetwork.mocks.errorMessage = nil
        
        sut.getCharacters { (characters, errorMessage) in
            expectation.fulfill()
            XCTAssertNotNil(characters)
            XCTAssertEqual(1, characters?.count)
            XCTAssertNil(errorMessage)
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testRequestData_IsFailure() {
        let expectation = self.expectation(description: "Fetch character failure")
        mockRequestNetwork.mocks.data = nil
        mockRequestNetwork.mocks.errorMessage = "Error"
        
        sut.getCharacters { (characters, errorMessage) in
            expectation.fulfill()
            XCTAssertNil(characters)
            XCTAssertNotNil(errorMessage)
            XCTAssertEqual("Error", errorMessage)
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    override func setUp() {
        super.setUp()
        mockRequestNetwork = MockRequestNetwork()
        sut = CharactersClient(network: mockRequestNetwork)
    }

    override func tearDown() {
        
    }
    
    var sut: CharactersClient!
    var mockRequestNetwork: MockRequestNetwork!
}
