//
//  CharactersViewModelTests.swift
//  BBCExplorerTests
//
//

import XCTest
import Fakery
@testable import BBCExplorer

class CharactersViewModelTests: XCTestCase {
    
    func testRequestCharacters_whenSuccess_updatesCharacters() {
        let expectation = self.expectation(description: "Characters success")
        mockClient.mocks.characters = Character.fakeCharacters()
        mockClient.mocks.errorMessage = ""
        
       sut.requestCharacters { (success: Bool, error: String?) in
           expectation.fulfill()
           XCTAssertTrue(success)
           XCTAssertNil(error)
       }
       waitForExpectations(timeout: 1.0, handler: nil)
       XCTAssertNotNil(sut.characters)
    }
    
    func testRequestCharacters_whenFails_updatesCharactersAsNil() {
        let expectation = self.expectation(description: "Characters failure")
        mockClient.mocks.characters = nil
        mockClient.mocks.errorMessage = "Error"
        
        sut.requestCharacters { (success: Bool, error: String?) in
            expectation.fulfill()
            XCTAssertFalse(success)
            XCTAssertNotNil(error)
        }
        waitForExpectations(timeout: 1.0, handler: nil)
        XCTAssertTrue(sut.characters.isEmpty)
    }
    
    func testAllCharacters_IsNotEmpty() {
        mockClient.mocks.characters = Character.fakeCharacters()
         mockClient.mocks.errorMessage = nil
        sut.requestCharacters { (success: Bool, error: String?) in
        }
        
        XCTAssertFalse(sut.allCharacters().isEmpty)
    }
    
    func testAllCharacters_IsEmpty() {
        mockClient.mocks.characters = nil
         mockClient.mocks.errorMessage = "Error"
        sut.requestCharacters { (success: Bool, error: String?) in
        }

        XCTAssertTrue(sut.allCharacters().isEmpty)
    }
    
    func testAllCharacters_CountIs2() {
        mockClient.mocks.characters = Character.fakeCharacters()
        mockClient.mocks.errorMessage = nil
        sut.requestCharacters { (success: Bool, error: String?) in
        }

        XCTAssertEqual(2, sut.allCharacters().count)
    }
    
    func testNavigateTo_Details() {
        sut.navigateTo(Character.fake())
        if let topViewController = self.navigationController.topViewController?.isKind(of: CharacterDetailsViewController.classForCoder()) {
            XCTAssertTrue(topViewController)
        }
    }
    
    func testCreateDelegateDataSource() {
        let dataSource = sut.createDelegateDatasource()
        XCTAssertTrue(dataSource.isKind(of: CharactersDelegateDataSource.classForCoder()))
    }
    
    func testCreateSearchController() {
        let searchController = sut.createSearchController()
        XCTAssertTrue(searchController.isKind(of: SearchController.classForCoder()))
    }

    override func setUp() {
        super.setUp()
        mockClient = MockCharactersClient()
        sut = CharactersViewModel(client: mockClient, navigationController: navigationController)
        _ = sut.createDelegateDatasource()
        _ = sut.createSearchController()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    var sut: CharactersViewModel!
    var mockClient: MockCharactersClient!
    let navigationController = UINavigationController()
}
