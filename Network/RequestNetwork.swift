//
//  FetchData.swift
//  BBCExplorer
//
//

import Foundation

protocol RequestNetworkType {
    func requestData(_ from: String, onCompletion: @escaping DataResult)
}

typealias DataResult = (_ data: Data?, _ errorMessage: String? ) -> Void

class RequestNetwork: RequestNetworkType {

    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?

    func requestData(_ from: String, onCompletion: @escaping DataResult) {
        guard let urlComponents = URLComponents(string: from), let url = urlComponents.url else {
             return
           }
         
        dataTask = defaultSession.dataTask(with: url) { [weak self] data, response, error in
         defer {
           self?.dataTask = nil
         }
         
         if let error = error {
            let errorMessage = "DataTask error: " + error.localizedDescription + "\n"
            onCompletion(nil, errorMessage)
         } else if
           let data = data,
           let response = response as? HTTPURLResponse,
           response.statusCode == 200 {
           DispatchQueue.main.async {
             onCompletion(data, nil)
           }
         }
       }
       dataTask?.resume()
    }
}
