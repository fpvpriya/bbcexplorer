//
//  ViewModelFactory.swift
//  BBCExplorer
//
//

import UIKit

protocol ViewModelFactoryType {
    func charactersFactory(with navigationController: UINavigationController) -> CharactersViewModelType
    func charactersDelegateDataSourceFactory(for parentViewModel: CharactersViewModel) -> CharactersDelegateDataSourceViewModelType
    func searchFactory(for parentViewModel: CharactersViewModel) -> SearchControllerViewModelType
    
    func characterDetailsFactory() -> CharacterDetailsViewModelType
}

class ViewModelFactory: ViewModelFactoryType {
    
    public static let shared = ViewModelFactory()
    
    //MARK: - Characters
    
    public func charactersFactory(with navigationController: UINavigationController) -> CharactersViewModelType {
        let client = CharactersClient(network: RequestNetwork())
        return CharactersViewModel(client: client, navigationController: navigationController)
    }
    
    //MARK: - CharactersTableView
    
    public func charactersDelegateDataSourceFactory(for parentViewModel: CharactersViewModel) -> CharactersDelegateDataSourceViewModelType {
        return CharactersDelegateDataSourceViewModel(parent: parentViewModel)
    }
    
    //MARK: - Search
    
    public func searchFactory(for parentViewModel: CharactersViewModel) -> SearchControllerViewModelType {
        return SearchControllerViewModel(parent: parentViewModel)
    }
    
    //MARK: - Character Details
    
    public func characterDetailsFactory() -> CharacterDetailsViewModelType {
        return CharacterDetailsViewModel()
    }
    
}
