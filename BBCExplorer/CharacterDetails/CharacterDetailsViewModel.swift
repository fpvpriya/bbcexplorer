//
//  CharacterDetailsViewModel.swift
//  BBCExplorer
//
//

import Foundation

protocol CharacterDetailsViewModelType {
    func setCharacter(_ character: Character)
    func characterName() -> String
    func characterOccupation() -> String
    func characterStatus() -> String
    func characterSeasons() -> String
    func characterImageURL() -> URL?
}

class CharacterDetailsViewModel: CharacterDetailsViewModelType {
    
    //MARK: - Properties
    
    var character: Character!
    
    func setCharacter(_ character: Character) {
        self.character = character
    }
    
    func characterName() -> String {
        return character.name + " ( " + character.nickname + " ) "
    }
    
    func characterOccupation() -> String {
        var occupations = ""
        for value in character.occupation {
            occupations += value
            if value != character.occupation.last {
                occupations += ", "
            }
            
        }
        return occupations
    }
    
    func characterStatus() -> String {
        return character.status
    }

    func characterSeasons() -> String {
        let seasonAppearanceAsString = character.seasonAppearance.map { String($0) }
        var seasonAppearances = ""
        for value in seasonAppearanceAsString {
            seasonAppearances += value
            if value != seasonAppearanceAsString.last {
                seasonAppearances += ", "
            }
        }
        return seasonAppearances
    }
    
    func characterImageURL() -> URL? {
        guard let urlComponents = URLComponents(string: character.imageString), let url = urlComponents.url else {
            return nil
        }
        return url
    }
}

