//
//  CharacterDetailsViewController.swift
//  BBCExplorer
//
//

import UIKit
import SDWebImage

class CharacterDetailsViewController: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var nameValue: UILabel!
    @IBOutlet weak var occupationValue: UILabel!
    @IBOutlet weak var statusValue: UILabel!
    @IBOutlet weak var seasonsValue: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    var viewModel: CharacterDetailsViewModelType!
    
    //MARK: - View functions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
    }
    
    //MARK: - Setup
    
    func setupData() {
        if let imageURL = viewModel.characterImageURL() {
                   image.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder.png"), options: SDWebImageOptions.refreshCached, completed: nil)
               }
        nameValue.text = viewModel.characterName()
        occupationValue.text = viewModel.characterOccupation()
        statusValue.text = viewModel.characterStatus()
        seasonsValue.text = viewModel.characterSeasons()
    }
}
