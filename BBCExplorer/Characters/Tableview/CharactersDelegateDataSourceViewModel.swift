//
//  CharactersDelegateDataSourceViewModel.swift
//  BBCExplorer
//
//

import UIKit

protocol CharactersDelegateDataSourceViewModelType {
    func setCharacters(_ characters: [Character])
    func allCharacters() -> [Character]
    
    func numberOfCharacters() -> Int
    func nameAtRow(_ row: Int) -> String
    func imageURLAtRow(_ row: Int) -> URL?
    func characterAtRow(_ row: Int) -> Character?
}

class CharactersDelegateDataSourceViewModel: CharactersDelegateDataSourceViewModelType {
    
    //MARK: - Properties
    
    var characters: [Character] = []
    weak var parent: CharactersViewModel!
    
    //MARK: - Initialization
    
    init(parent: CharactersViewModel) {
        self.parent = parent
    }
    
    func setCharacters(_ characters: [Character]) {
        self.characters = characters
    }
    
    func allCharacters() -> [Character] {
        return self.characters
    }
    
    func numberOfCharacters() -> Int {
        return self.characters.count
    }
    
    func nameAtRow(_ row: Int) -> String {
        if !characters.isEmpty {
            let character: Character = characters[row]
            return character.name
        }
        return ""
    }
    
    func imageURLAtRow(_ row: Int) -> URL? {
        if !characters.isEmpty {
            let character: Character = characters[row]
            guard let urlComponents = URLComponents(string: character.imageString), let url = urlComponents.url else {
                return nil
            }
            return url
        }
        return nil
    }
    
    func characterAtRow(_ row: Int) -> Character? {
        if !characters.isEmpty {
            return characters[row]
        }
        return nil
    }
}
