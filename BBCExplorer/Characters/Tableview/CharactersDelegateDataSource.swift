//
//  CharactersDelegateDataSource.swift
//  BBCExplorer
//
//

import UIKit
import SDWebImage

class CharacterCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var characterImage: UIImageView!
}

class CharactersDelegateDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Properties
    
    var viewModel: CharactersDelegateDataSourceViewModelType!
    var selectCharacter : ((Character) -> Void)?
    
    //MARK: - Initialization
    
    init(viewModel: CharactersDelegateDataSourceViewModelType) {
        self.viewModel = viewModel
    }
    
    //MARK: - Tableview Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfCharacters()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterCell", for: indexPath) as! CharacterCell
        cell.name.text = viewModel.nameAtRow(indexPath.row)
        if let imageURL = viewModel.imageURLAtRow(indexPath.row) {
            cell.characterImage.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder.png"), options: SDWebImageOptions.refreshCached, completed: nil)
        }
        return cell
    }
    
    //MARK: - Tableview Delegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectCharacter = selectCharacter, let character = viewModel.characterAtRow(indexPath.row) {
            selectCharacter(character)
        }
    }
    
}
