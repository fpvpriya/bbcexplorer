//
//  CharactersViewController.swift
//  BBCExplorer
//
//

import UIKit

class CharactersViewController: UIViewController {

    //MARK: - Properties
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var viewModel: CharactersViewModelType!
    var charactersDelegateDataSource: CharactersDelegateDataSource!
    var searchController: SearchController!
        
    //MARK: - View functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        loadCharacters()
    }
    
    //MARK: - Setup
    
    func setup() {
        setupViewModel()
        setupDelegateDatasourceViewModel()
        setupSearchController()
    }
    
    func setupViewModel() {
        self.viewModel = ViewModelFactory.shared.charactersFactory(with: self.navigationController ?? UINavigationController())
    }
    
    func setupDelegateDatasourceViewModel() {
        self.charactersDelegateDataSource = viewModel.createDelegateDatasource()
        self.charactersDelegateDataSource.selectCharacter = {(character) in
            self.viewModel.navigateTo(character)
        }
        self.tableView.delegate = self.charactersDelegateDataSource
        self.tableView.dataSource = self.charactersDelegateDataSource
    }
    
    func setupSearchController() {
        self.searchController = viewModel.createSearchController()
        navigationItem.searchController = searchController
        self.searchController.updateTableView = {
            self.tableView.reloadData()
        }
    }
    
    //MARK: - Load chacters
    
    func loadCharacters() {
        viewModel.requestCharacters { (success, error) in
            self.activityIndicator.stopAnimating()
            if success {
                self.tableView.reloadData()
            }
        }
    }
    
}

