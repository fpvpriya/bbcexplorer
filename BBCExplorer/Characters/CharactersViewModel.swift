//
//  CharactersViewModel.swift
//  BBCExplorer
//
//

import UIKit

public typealias CompletionHandler = (_ success: Bool, _ errorMessage: String?) -> Void

protocol CharactersViewModelType {
    func allCharacters() -> [Character]
    func requestCharacters(onCompletion: @escaping CompletionHandler)

    func createDelegateDatasource() -> CharactersDelegateDataSource
    func navigateTo(_: Character)
    
    func createSearchController() -> SearchController
    func updateFilteredCharacters(_ characters: [Character])
}

class CharactersViewModel: CharactersViewModelType {
    
    //MARK: - Properties
    
    let client: CharactersClientType!
    let navigationController: UINavigationController!
    var characters: [Character] = []
    var tableViewViewModel: CharactersDelegateDataSourceViewModelType!
    var searchViewModel: SearchControllerViewModelType!
    
    init(client: CharactersClientType, navigationController: UINavigationController) {
        self.client = client
        self.navigationController = navigationController
    }
    
    //MARK: - RequestCharacters
    
    func requestCharacters(onCompletion: @escaping CompletionHandler) {
        client.getCharacters { (characters, errorMessage) in
            guard let characters = characters else {
                self.characters = []
                onCompletion(false, "No characters")
                return
            }
            self.characters = characters
            self.updateTableViewModel(characters)
            self.updateSearchViewMode(characters)
            onCompletion(true, nil)
        }
    }
    
    func allCharacters() -> [Character] {
        return self.characters
    }
    
    func updateFilteredCharacters(_ characters: [Character]) {
        self.tableViewViewModel.setCharacters(characters)
    }
    
    //MARK: - CharacterDetails
    
    func navigateTo(_ character: Character) {
        let viewModel: CharacterDetailsViewModel = ViewModelFactory.shared.characterDetailsFactory() as! CharacterDetailsViewModel
        viewModel.character = character
        let sb = UIStoryboard(name: "Main", bundle: .main)
        let viewController: CharacterDetailsViewController = sb.instantiateViewController(withIdentifier: "CharacterDetailsViewController") as! CharacterDetailsViewController
        viewController.viewModel = viewModel
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    //MARK: - Private functions
    
    private func updateTableViewModel(_ characters: [Character]) {
           self.tableViewViewModel.setCharacters(characters)
       }
    
    private func updateSearchViewMode(_ characters: [Character]) {
        self.searchViewModel.setAllCharacters(characters)
    }
    
    //MARK: - ViewModelFactory
    
    func createDelegateDatasource() -> CharactersDelegateDataSource {
        self.tableViewViewModel = ViewModelFactory.shared.charactersDelegateDataSourceFactory(for: self)
        return CharactersDelegateDataSource(viewModel: tableViewViewModel)
    }
    
    func createSearchController() -> SearchController {
        self.searchViewModel = ViewModelFactory.shared.searchFactory(for: self)
        let searchController = SearchController.init(searchResultsController: nil, viewModel: self.searchViewModel)
        return searchController
    }

}
