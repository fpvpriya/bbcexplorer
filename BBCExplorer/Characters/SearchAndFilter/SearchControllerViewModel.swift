//
//  SearchControllerViewModel.swift
//  BBCExplorer
//
//

import UIKit

protocol SearchControllerViewModelType {
    func filterContentForSearchText(_ searchText: String, _ isSearchBarEmpty: Bool, _ selectedScope: Int)
    func setAllCharacters(_ characters: [Character])
}

class SearchControllerViewModel: SearchControllerViewModelType {
    
    //MARK: - Properties
    
    var allCharacters: [Character] = []
    weak var parent: CharactersViewModel!
    
    //MARK: - Initialization
    
    init(parent: CharactersViewModel) {
        self.parent = parent
    }
    
    func setAllCharacters(_ characters: [Character]) {
        self.allCharacters = characters
    }
    
    func filterContentForSearchText(_ searchText: String, _ isSearchBarEmpty: Bool, _ selectedScope: Int) {
        let filteredCharacters = self.allCharacters.filter { (character: Character) -> Bool in
            if !isSearchBarEmpty && selectedScope == 0 {
                return character.name.lowercased().contains(searchText.lowercased())
            } else if !isSearchBarEmpty && selectedScope >= 1 {
                return (character.name.lowercased().contains(searchText.lowercased()) &&
                character.seasonAppearance.contains(selectedScope))
            } else if isSearchBarEmpty && selectedScope == 0 {
                return true
            } else if isSearchBarEmpty && selectedScope >= 0 {
                return character.seasonAppearance.contains(selectedScope)
            }
            else {
                return true
            }
      }
        parent.updateFilteredCharacters(filteredCharacters)
    }
}
