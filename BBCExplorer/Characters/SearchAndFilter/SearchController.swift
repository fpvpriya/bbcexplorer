//
//  SearchController.swift
//  BBCExplorer
//
//

import UIKit

class SearchController: UISearchController, UISearchResultsUpdating, UISearchBarDelegate {
    
    //MARK: - Properties
    var viewModel: SearchControllerViewModelType!
    var updateTableView: (() -> Void)?
    var selectedScope: Int = 0
    
    //MARK: - Initialization
    
    init(searchResultsController: UIViewController?, viewModel: SearchControllerViewModelType) {
        super.init(searchResultsController: nil)
        self.viewModel = viewModel
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Configure
    
    func configure() {
        searchResultsUpdater = self
        searchBar.delegate = self
        obscuresBackgroundDuringPresentation = false
        searchBar.placeholder = "Search Characters"
        searchBar.scopeButtonTitles = ["All", "1", "2", "3", "4", "5"]
        definesPresentationContext = true
    }
    
    var isSearchBarEmpty: Bool {
       return searchBar.text?.isEmpty ?? true
     }
    
    //MARK: - UISearchResultsUpdating Delegate
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContent(searchText: searchBar.text!, selectedScope: selectedScope)
     }
    
    //MARK: - UISearchBar Delegate

    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        self.selectedScope = selectedScope
        filterContent(searchText: searchBar.text!, selectedScope: selectedScope)
    }
    
    func filterContent(searchText: String, selectedScope: Int) {
        viewModel.filterContentForSearchText(searchBar.text!, isSearchBarEmpty, selectedScope)
        if let updateTableView = updateTableView {
            updateTableView()
        }
    }
}
